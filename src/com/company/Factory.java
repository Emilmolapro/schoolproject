package com.company;

public class Factory {
    private int numberOfStudents;
    private int numberOfProjects;

    public Factory(){
        numberOfStudents=0;
        numberOfProjects=0;
    }

    public Student addStudent(String name, int age){
        numberOfStudents++;
        return new Student(numberOfStudents, name, age);
    }

    public Project addProject(String name){
        numberOfProjects++;
        return new Project(numberOfStudents, name);
    }
}
