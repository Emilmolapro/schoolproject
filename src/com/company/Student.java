package com.company;

public class Student {
    private int id;
    private String name;
    private int age;
    private Project project;


    public Student(int id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    @Override
    public String toString() {
        if(project==null)
        return "Student [ID: "+id+", Name: "+name+", Age: "+age+']';
        else{ return "Student [ID: "+id+", Name: "+name+", Age: "+age+", Assigned to: "+']'; }
    }
}
