package com.company;

import java.util.Scanner;

public class Start {
    private Storage<Student>studentStorage;
    private Storage<Project>projectStorage;
    public Factory factory;

    private void defaultMenuMessage(){
        System.out.println("Please choose an option by ID");
    }

    private void defaultErrorMessage(){
        System.out.println("ERROR: Invalid choice");
        return;
    }

    public void startMenu(){
        Scanner scanner = new Scanner(System.in);
        boolean stay = true;
        defaultMenuMessage();
        System.out.println("0: Exit || 1: Add items || 2: Remove items || 3: Print || 4: Assign");
        System.out.print("You choose: ");
        int choice = scanner.nextInt(); scanner.nextLine();

        while(stay){
            switch (choice){
                case 0: stay = false; break;
                case 1: addItem(); break;
                case 2: removeItem(); break;
                case 3: print(); break;
            }
        }
    }

    private void print() {
        Scanner scanner = new Scanner(System.in);
        System.out.println();
        defaultMenuMessage();
        System.out.println("0: Go back || 1: Print Student list || 2: Print project list || 3: Print all");
        System.out.print("You choose: ");
        int choice = scanner.nextInt(); scanner.nextLine();

        switch (choice){
            case 0: break;
            case 1: printStudents();
            case 2: printProjects();
            case 3: printAll();
            default: defaultErrorMessage();
        } return;
    }

    private void printAll() {
        printStudents();
        printProjects();
    }

    private void printProjects() {
        System.out.println();
        System.out.println("List of projects:");
        for (int i = 0; i < projectStorage.getSize(); i++) {
            System.out.println(projectStorage.getIndex(i));
        }

    }

    private void printStudents() {
        System.out.println();
        System.out.println("List of students:");
        for (int i = 0; i < studentStorage.getSize(); i++) {
            System.out.println(studentStorage.getIndex(i));
        }
    }

    private void removeItem() {
        Scanner scanner = new Scanner(System.in);
        System.out.println();
        System.out.println("0: Go Back || 1: Remove a Student || 2: Remove a Project");
        System.out.print("You choose: ");
        int choice = scanner.nextInt(); scanner.nextLine();

        switch (choice){
            case 0: break;
            case 1: removeStudent(); break;
            case 2: removeProject(); break;
            default: defaultErrorMessage(); break;
        }
        return;
    }

    private void removeProject() {
        Scanner scanner = new Scanner(System.in);
        printProjects();
        System.out.println("Choose project by ID number");
        System.out.print("You choose: ");
        int index = scanner.nextInt(); scanner.nextLine();

        if(projectStorage.contains(index)){
            removeProject();
        }
        else{
            defaultErrorMessage(); return;
        }
    }

    private void removeStudent() {
        Scanner scanner = new Scanner(System.in);
        printProjects();
        System.out.println("Choose student by ID number");
        System.out.print("You choose: ");
        int index = scanner.nextInt(); scanner.nextLine();

        if(studentStorage.contains(index)){
            removeProject();
        }
        else{
            defaultErrorMessage(); return;
        }
    }

    private void addItem() {
        System.out.println();
        Scanner scanner = new Scanner(System.in);
        defaultMenuMessage();
        System.out.println("0: Go back || 1: Add Student || 2: Add Project");
        System.out.print("You choose: ");
        int choice = scanner.nextInt(); scanner.nextLine();

        switch (choice){
            case 0: break;
            case 1: addStudent(); break;
            case 2: addProject(); break;
            default: defaultErrorMessage(); break;

        }
    }

    private void addStudent(){
        Scanner scanner = new Scanner(System.in);
        System.out.println();
        System.out.print("Enter name: ");
        String name = scanner.nextLine();
        System.out.print("Enter age: ");
        int age = scanner.nextInt(); scanner.nextLine();

        Student student = factory.addStudent(name, age);
        studentStorage.addItem(student);
    }

    private void addProject(){
        Scanner scanner = new Scanner(System.in);
        System.out.println();
        System.out.print("Enter name: ");
        String name = scanner.nextLine();

        Project project = factory.addProject(name);
        projectStorage.addItem(project);
    }
}
