package com.company;

public class Project {
    private int id;
    private String name;

    public Project(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String toString(){
        return "Project [ID: "+id+", Name: "+name+']';
    }
}
