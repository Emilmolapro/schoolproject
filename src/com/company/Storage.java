package com.company;

import java.util.ArrayList;
import java.util.Comparator;

public class Storage<T> {
    private ArrayList<T> items;

    public Storage(){this.items = new ArrayList<>();}

    public void addItem (T item){items.add(item);}
    public void removeItemByIndex (int index){items.remove(index);}

    public T getIndex(int index){return items.get(index);}
    public int getSize(){return items.size();}
    public void sortItems(Comparator comparator){items.sort(comparator);}
    public boolean isEmpty(){return items.isEmpty();}
    public boolean contains(int index){return items.contains(index);}


}
